 ID: 32_fma_MIN_METADATA_dataset
 Name: fma dataset
 Description: The data consists small subset of raw music files. The audio files have different bit rate and length. All are mp3 format
 License: Creative Commons Attribution 4.0 International License (CC BY 4.0)
 License Link: https://creativecommons.org/licenses/by/4.0
 Source: MIT license
 Source Link: https://github.com/mdeff/fma
 Citation: 
